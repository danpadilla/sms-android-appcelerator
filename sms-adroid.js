var win1 = Titanium.UI.createWindow({
    title:'Tab 1',
    backgroundColor:'#fff',
    layout: 'vertical'
});


var bt1 = Ti.UI.createButton({title: 'send message (method 1)', top: 200});

bt1.addEventListener('click', function(e)
{
	var intent = Ti.Android.createIntent({
		action: Ti.Android.ACTION_SENDTO,
		data: 'smsto:123456789'
	});
	intent.putExtra('sms_body', 'new message from me');
	Ti.Android.currentActivity.startActivity(intent);
});

win1.add(bt1);

var bt2 = Ti.UI.createButton({title: 'send message (method 2)'});
bt2.addEventListener('click', function(e)
{
	var intent = Ti.Android.createIntent({
		action: Ti.Android.ACTION_VIEW,
		type: 'vnd.android-dir/mms-sms'
	});
	intent.putExtra('sms_body', 'new message from me');
	intent.putExtra('address', '123456789');
	Ti.Android.currentActivity.startActivity(intent);
});

win1.add(bt2);

win1.open();
